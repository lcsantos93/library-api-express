const mongoose = require('mongoose');

const BookSchema = mongoose.Schema({
    "title": {
        type: String,
        required: true
    },
    "ISBN": {
        type: String,
        required: false
    },
    "category": {
        type: String,
        required: true
    },
    "year": {
        type: String,
        required: true
    }
}, {
    timestamps: true
}, {collection: 'bookcollection'});

module.exports = mongoose.model('Book', BookSchema);