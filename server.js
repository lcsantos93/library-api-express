let express = require('express')
let app = express()
let swig = require('swig') 
const bodyParser = require('body-parser');
let path = require('path');
let cons = require('consolidate')
//let bootstrap = require('bootstrap')
let jquery = require('jquery')
let popper = require('popper.js')
let port = 3000
const ip = 'localhost'


let routes = require('./routes/index');

// app.engine('.html', require('ejs').__express);
app.engine('html', cons.swig)
app.set('view engine', 'html');

app.set('resources',__dirname+'/resources');
app.set('node_modules', __dirname+'/node_modules');

app.use('/scripts', express.static('node_modules/bootstrap/dist/'));
app.use('/scripts', express.static('node_modules/jquery/dist/'));    
app.use('/scripts', express.static('node_modules/popper.js/dist/'));
app.use('/glidejs', express.static('node_modules/@glidejs/glide/dist'));

app.use('/css', express.static('resources/css'));
app.use('/images', express.static('resources/images'));
app.use('/svg', express.static('resources/svg'));
app.use('/js', express.static('resources/js'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', routes);


// Configuring the database
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, { useNewUrlParser: true })
.then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.use( (req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.listen(port, ip, () => {
  console.log(`Servidor rodando em http://${ip}:${port}`)
  console.log('Para derrubar o servidor: ctrl + c');
})    


module.exports = app;