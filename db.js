const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/library-api', { useNewUrlParser: true });

const bookSchema = new mongoose.Schema(const BookSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    ISBN: {
        type: String,
        required: false
    },
    category: {
        type: String,
        required: true
    },
    year: {
        type: String,
        required: true
    },
 
    timestamps: true
}, {collection: 'bookcollection'});

module.exports = { Mongoose: mongoose, BookSchema: bookSchema }