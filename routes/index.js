let express = require('express'),
  router = express.Router(),
  assert = require('assert')

const books = require('../controllers/book.controller.js');

// Create a new Book
router.post('/books', books.create);

// Retrieve all Books
router.get('/books', books.findAll);

// Retrieve a single Book with bookId
router.get('/books/:bookId', books.findOne);

// Update a Book with bookId
router.put('/books/:bookId', books.update);

// Delete a Book with bookId
router.delete('/books/:bookId', books.delete);


router.get('/', (req, res) => {
  res.render('index')
})

router.get('/carousel', (req, res) => {
  res.render('carousel')
})


router.get('/examples', (req, res) => {
  res.render('examples')
})

router.get('/booklist', (req, res) => {
  // let db = require("../db");
  let db = require("../models/book.model");
  let Books = db.Mongoose.model('bookcollection', db.BookSchema, 'bookcollection');
  Books.find({}).lean().exec((e, docs) => {
    res.render('bookList', {
      'bookList': docs
    });
    console.log(bookList);
    //res.render('user')

  });
});
module.exports = router;